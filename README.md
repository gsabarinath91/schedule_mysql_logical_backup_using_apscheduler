# README #

It's all about an approach to run schedule backup of MySQL DB by utilizing python library and avoid depending privileged system tool cron.

### What is this repository for? ###

A python way to schedule the mysql logical backups using APScheduelr and avoid dependency of system cron.

### How do I get set up? ###

* Python 2.7.5
* Execution
python mysqlbackup.py --help              =     Know valid command argument supported
python mysqlbackup.py -passwd `cat passwd` -user bkpuser       =   execute by user and passwd supplied in file with permission of 600
python mysqlbackup.py -passwd `cat passwd` -user bkpuser -db subquery_w3     =  backup specific db
python mysqlbackup.py -P `cat passwd` --location='/tmp' --conf='schedule.conf'     =    run schedule backup 

Many of the argument will be taken default if not explicitly defined. 

### Contribution guidelines ###

* Try it,and produce json for schedule configuration


### Who do I talk to? ###

*sabarinath G
*g.sabarinath91@gmail.com