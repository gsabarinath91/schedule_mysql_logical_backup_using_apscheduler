import argparse
import MySQLdb as mysql
import os
import sys
import contextlib
import socket
from apscheduler.schedulers.background import BackgroundScheduler
import time
from datetime import datetime
import ast
import six

#Instancitiate of background scheduler class
scheduler=BackgroundScheduler()

#command argument parser
parser = argparse.ArgumentParser(description='Establish MYSQL connection with either supplied argument or use default.')
parser.add_argument('--host','-H',default='localhost',action='store',help='hostname or ip address of db host,default is "localhost"')
parser.add_argument('--user','-U',default='root',action='store',help='username to connect mysql db instance,default is "root"')
parser.add_argument('--passwd','-P',default='None',action='store',help='password of supplied user,default is "None"')
parser.add_argument('--db','-DB',default='mysql',action='store',help='db name to connect,default is "mysql"')
parser.add_argument('--port','-p',default=3306,action='store',help='TCP/IP port listening by mysqld,default is 3306')
parser.add_argument('--connect-timout',action='store',help='limit active connection timeout')
parser.add_argument('--location',default=os.getcwd(),action='store',help='storage location for backup,default is current working directory')
parser.add_argument('--schedule',default='date',action='store',type=str,choices=['date','cron'],help='choose job scheduler either date or cron,default is "date"')
parser.add_argument('--conf',default=os.path.realpath('schedule.conf'),action='store',help='schedule configuration file location,default schedule.conf lying current wor
king directory')

#Effect the argument by instance and print
dbarg= parser.parse_args()
print 'DB host : ',dbarg.host
print 'DB user : ',dbarg.user
print 'DB passwd : ',dbarg.passwd
print 'DB name : ',dbarg.db
print 'DB port : ',dbarg.port
print 'Backup location : ',dbarg.location
print 'Schedule : ',dbarg.schedule
print 'Conf : ',dbarg.conf

#Return SQL query to check user priviliege
def priv_query(host=None):
    #get_user_priv = 'select GRANTEE,PRIVILEGE_TYPE from information_schema.user_privileges where GRANTEE="\'{}\'@\'{}\'"'.format(dbarg.user,host)
    get_user_priv = 'select GRANTEE,PRIVILEGE_TYPE from information_schema.user_privileges where (GRANTEE = "\'{}\'@\'{}\'") and (PRIVILEGE_TYPE = \'{}\' or PRIVILEGE_T
YPE = \'{}\' or PRIVILEGE_TYPE = \'{}\')'.format(dbarg.user,host,'SELECT','SHOW VIEW','LOCK TABLES')
    return get_user_priv

#MYSQL DB connection handle
class DB_handle():
    def __init__(self,sqlquery,mysqldb):
        self.mysql_cur = mysqldb.cursor()
        self.sqlquery = sqlquery
    def cnx_open(self):
        self.mysql_cur.execute(self.sqlquery)
        self.query_out = self.mysql_cur.fetchall()
        return self.query_out
    def close(self):
        self.mysql_cur.close()


#Establish DB connection and execute the SQL query ,also handle connection error
def task(query):
    try:
        mysqldb = mysql.connect(dbarg.host,dbarg.user,dbarg.passwd,dbarg.db,dbarg.port)
        try:
            with contextlib.closing(DB_handle(query,mysqldb)) as db:
              #  print db.cnx_open()
                return db.cnx_open()
        except  mysql.ProgrammingError as prgerr:
            print 'MYSQL PROGRAM ERR:{}'.format(prgerr)
        print 'DB connected successfully and exited'
    except mysql.InterfaceError as pyerr:
        print 'MYSQL INTERFACE ERR:{}'.format(pyerr)
    except mysql.OperationalError as operaterr:
        print 'MYSQL OPERATIONAL ERR:{}'.format(operaterr)
    except mysql.InternalError as inerr:
        print 'MYSQL INTERNAL ERR:{}'.format(inerr)
    except mysql.DataError as dataerr:
        print 'MYSQL DATA ERROR:{}'.format(dataerr)
    except mysql.Error as err:
        print 'MYSQL GENERIC ERROR:{}'.format(err)
#Finding the client host IP,by using socket connection
class find_client_network_info(object):
    def __init__(self):
        self.Socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def client_ip(self):
        try:
            self.Socket.connect((str(dbarg.host),int(dbarg.port)))
            client_ip=self.Socket.getsockname()[0]
            return client_ip
        except socket.error as socerr:
            print "SOCKET ERR:{}".format(socerr)
        except socket.herror as addrerr:
            print "SOCKET ADDR ERR:{}".format(addrerr)
        except socket.gaierror as neterr:
            print "SOCKET NETWORK ERR:{}".format(neterr)
        except socket.error as generr:
            print "SOCKET GENERIC ERR:{}".format(generr)
        finally:
            print 'client {} Socket connection closed'.format(client_ip)
            self.Socket.close()
    def client_hostname(self):
        return os.getenv('HOSTNAME')

#A subclass,return user privillege with respect to network parameter of IP and Hostname
class Privilege(find_client_network_info):
    def __init__(self,default='localhost'):
        super(Privilege,self).__init__()
        self.client_ip = super(Privilege,self).client_ip()
        self.client_hostname = super(Privilege,self).client_hostname()
        self.default_hostname = default
    def check_user_privilege_by_ip(self):
        sql_query = priv_query(host=self.client_ip)
        sql_query_out=task(sql_query)
        sql_query_out_list = []
        for i in range(len(sql_query_out)):
                sql_query_out_list = sql_query_out_list + list(sql_query_out[i])
        print 'check_user_privilege_by_ip',sorted(list(set(sql_query_out_list)))
        return sorted(list(set(sql_query_out_list)))
    def check_user_privilege_by_hostname(self):
        sql_query= priv_query(host=self.client_hostname)
        sql_query_out=task(sql_query)
        sql_query_out_list = []
        for i in range(len(sql_query_out)):
                sql_query_out_list = sql_query_out_list + list(sql_query_out[i])
        print 'check_user_privilege_by_hostname',sorted(list(set(sql_query_out_list)))
        return sorted(list(set(sql_query_out_list)))
    def check_user_privilege_by_localhost(self):
        sql_query= priv_query(host=self.default_hostname)
        sql_query_out=task(sql_query)
        sql_query_out_list = []
        for i in range(len(sql_query_out)):
                sql_query_out_list = sql_query_out_list + list(sql_query_out[i])
        print 'check_user_privilege_by_localhost',sorted(list(set(sql_query_out_list)))
        return sorted(list(set(sql_query_out_list)))

priv_inst = Privilege()
priv_query_out = [priv_inst.check_user_privilege_by_ip(),priv_inst.check_user_privilege_by_hostname(),priv_inst.check_user_privilege_by_localhost()]
print priv_query_out

#Function to verify user against defined permission attributes
def priv_check(priv_query_out):
    org_priv = [['LOCK TABLES', 'SELECT', 'SHOW VIEW'],['SELECT', 'SHOW VIEW']]
    priv_result = False
    for i in range(len(priv_query_out)):
            if len(priv_query_out[i]) >0:
                if org_priv.__contains__(priv_query_out[i][1:]):
                        priv_result=True
    print "User has priviliege to run backup: {}".format(priv_result)
    return priv_result

#File descriptor handler
class File_d():
    def __init__(self,file_name,method):
        self.file_obj = open(file_name,method)
    def __enter__(self):
        return self.file_obj.read()
    def __exit__(self):
        self.file_obj.close()
#Function to produce necessary sleep to trigger date schedule job
def sleep_wait_background():
    read_schedule_conf = File_d(dbarg.conf,'r')
    sleep_seconds = time.mktime(time.strptime(read_schedule_conf.__enter__().split('\n')[0],'%Y-%m-%d %H:%M:%S'))- time.mktime(time.strptime(datetime.now().strftime('%Y
-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S'))
    return sleep_seconds+120

#APScheduler class
class backup_schedule():
    def __init__(self):
        self.schedule=dbarg.schedule
        self.scheduler = scheduler
        self.scheduler_conf = dbarg.conf
    def date(self,dbbackup_job):
        occurence = File_d(self.scheduler_conf,'r')
        self.scheduler.add_job(dbbackup_job,'date',run_date=occurence.__enter__())
        occurence.__exit__()
        print "Starting date scheduler Job..."
        self.scheduler.start()
    def cron(self,dbbackup_job):
        with open(self.scheduler_conf,'r') as schd_json:
                cron_d = ast.literal_eval(schd_json.read())
        self.scheduler.add_job(dbbackup_job, 'cron',year=cron_d['cron']['year'],month=cron_d['cron']['month'],week=cron_d['cron']['week'],day=cron_d['cron']['day'],day_
of_week=cron_d['cron']['day_of_week'],hour=cron_d['cron']['hour'],minute=cron_d['cron']['minute'], second= cron_d['cron']['start_date'], start_date=cron_d['cron']['star
t_date'],end_date=cron_d['cron']['end_date'],timezone=cron_d['cron']['timezone'])
        running = True
        print ('Starting Cron scheduler Job...')
        self.scheduler.start()
        try:
           while running:
                time.sleep(1)
                for s in sorted(six.iteritems(self.scheduler._jobstores)):
                        jobs = s[1].get_all_jobs()
                if jobs == []:
                        running =False
                        print "No pending scheduler job"
        except (KeyboardInterrupt, SystemExit):
                print 'The program has been interrupted... closing'
def backup_job():
    if priv_check(priv_query_out) == True:
        fd=os.popen("mysqldump -u %s --password='%s' -h %s --all-databases > %s/%s-%s.sql" % (dbarg.user, dbarg.passwd, dbarg.host,dbarg.location,dbarg.db,datetime.now(
).strftime('%Y-%b-%d-%H%M%S')))
        fd.close()
        return False
    else:
        print 'User had no SQL permissions to run Backup job'
        sys.exit(1)


if __name__ == '__main__':
    try:
        bkp=backup_schedule()
        if dbarg.schedule == 'date':
                bkp.date(backup_job)
                time.sleep(sleep_wait_background())
                print 'Lease time for schedule Job',sleep_wait_background()
        elif dbarg.schedule == 'cron':
                bkp.cron(backup_job)
                print 'RUNNING CRON JOB'
        else:
               print 'No valid schedule declared'
    finally:
        print 'Backup completed'
        bkp.scheduler.shutdown()